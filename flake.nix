{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    material-cursors-git = {
      url = "github:varlesh/material-cursors";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, material-cursors-git, ... }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      pname = "material-cursors";
    in rec {
      packages.x86_64-linux.${pname} = pkgs.stdenv.mkDerivation {
        inherit pname;
        version = material-cursors-git.rev;
        src = material-cursors-git;
        buildInputs = with pkgs; [ git inkscape libcanberra xorg.xcursorgen ];
        buildPhase = "export NO_AT_BRIDGE=1; make build";
        installPhase = "make DESTDIR=\"$out\" install; mv $out/usr/share $out/; rmdir $out/usr";
      };

      packages.x86_64-linux.default = packages.x86_64-linux.${pname};
    };
}

